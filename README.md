# inverse-perspective-mapping

Small project that warps a given image into a `birds eye perspective` (top down view) by applying an inverse perspective mapping (IPM).  
Requires the relative transformation between the camera frame and a ground frame on the floor (plane to project on).
Inverse perspective mapping can be used to rectify distorted images (right angels and cirles appear undistorted in the final image).

Requires `opencv3`. Build project with qt-creator.

## Functions
* Call `update` function whenever a new Transformation (roll, pitch, yaw, origin) between camera and floor becomes available (use ros tf tree)
* Use `warp` to generate a warped image
* use `pixel2World` / `world2pixel` to map between 3d world coordinates and image pixel coordinates

![Alt Text](ipm.png)


