#include "ipm.h"
#include <iostream>
#include <opencv2/core/eigen.hpp>

#include "polygon_clipping.h"

using namespace ipm;

IPM::IPM(const Matrix& K) : K(K)
{
    // 1. init camera matrix
    P = build_P(K, Matrix::Identity(3,3), Matrix::Zero(3,1));
    invP = build_invP(K);

    // 2. init camera to world transformation
    PC_W = Matrix::Zero(4, 3);
    PW_C = Matrix::Zero(3, 4);

    // 3. init camera desired to world transformation
    PCd_W = Matrix::Zero(4, 3);
    PW_Cd = Matrix::Zero(3, 4);

    // 3. setup desired camera
    RCd_W = Matrix(3,3); Cd_W = ipm::Vector(3);
    RCd_W << 0, -1, 0, -1, 0, 0, 0, 0, -1;
    HCd_W = build_H(RCd_W, Matrix::Zero(3,1));

    roi.push_back( {2500, 1000} );
    roi.push_back( {2500, -1000} );
    roi.push_back( {0, -1000} );
    roi.push_back( {0, 1000} );
}

void IPM::set_K( const Matrix& K )
{
    this->K = K;
    P = build_P(K, Matrix::Identity(3,3), Matrix::Zero(3,1));
    invP = build_invP(K);
}

void IPM::set_roi( std::vector<cv::Point2f> roi )
{
    this->roi = roi;
}

void IPM::update(float roll, float pitch, float yaw, const ipm::Vector& center)
{
    // 1. compute camera to world transformation
    Matrix RC_W = build_R(roll, pitch, yaw);
    Matrix HC_W = build_H(RC_W, center);

    // 2. Projection Matrix for planar floor surface
    ipm::Vector o_C = -RC_W.transpose() * center;
    ipm::Vector n_W(3); n_W << 0, 0, 1;
    ipm::Vector n_C= RC_W.transpose() * n_W;
    float d = (n_C.transpose() * o_C)[0];

    Matrix S(4, 4);
    S << Matrix::Identity(3,3), Matrix::Zero(3,1), n_C[0] / d, n_C[1] / d, n_C[2] / d, 0;

    // 3. Combine Projection matrix and camera matrix ( C to W )
    PC_W = HC_W * S * invP;
    PW_C = P * HC_W.inverse();

    // 4. compute homography form C to Cd
    HCd_W = build_H(RCd_W, center);
    H = P * HCd_W.inverse() * PC_W;
}

void IPM::pixel2World(const std::vector< cv::Point2f >& x, std::vector< cv::Point3f >& X)
{
    Matrix hom_image_pts, hom_world_pts;
    to_homogeneous(x, hom_image_pts);
    hom_world_pts = PC_W * hom_image_pts;
    from_homogeneous(hom_world_pts, X);
}

void IPM::warp_pixel2World(const std::vector< cv::Point2f >& x, std::vector< cv::Point3f >& X)
{
    Matrix hom_image_pts, hom_world_pts;
    to_homogeneous(x, hom_image_pts);
    hom_world_pts = PCd_W * hom_image_pts;
    from_homogeneous(hom_world_pts, X);
}

void IPM::world2Pixel(const std::vector< cv::Point3f >& X, std::vector< cv::Point2f >& x)
{
    Matrix hom_world_pts, hom_image_pts;
    to_homogeneous(X, hom_world_pts);
    hom_image_pts = PW_C * hom_world_pts;
    from_homogeneous(hom_image_pts, x);
}

void IPM::warp_world2Pixel(const std::vector< cv::Point3f >& X, std::vector< cv::Point2f >& x)
{
    Matrix hom_world_pts, hom_image_pts;
    to_homogeneous(X, hom_world_pts);
    hom_image_pts = PW_Cd * hom_world_pts;
    from_homogeneous(hom_image_pts, x);
}

void IPM::warp(const cv::Mat& src, cv::Mat& dst)
{
    Matrix corners_C(3, 4);
    corners_C << 0, src.cols, src.cols, 0, 0, 0, src.rows, src.rows, 1, 1, 1, 1;
    Matrix corners_C_W = PC_W * corners_C;

    // image corners in world coordinates
    std::vector<cv::Point2f> corners_C_W_vec(corners_C_W.cols());
    for(int i = 0; i < corners_C_W_vec.size(); ++i) {
        corners_C_W_vec[i].x = corners_C_W(0, i) / corners_C_W(3, i);
        corners_C_W_vec[i].y = corners_C_W(1, i) / corners_C_W(3, i);
    }
    // region of interest in world coordinates
    std::vector<cv::Point2f> rect;
    suthHodgClip(corners_C_W_vec, roi, rect);

    // region of interest in World coordinates
    Matrix corners_roi_W_h(4, rect.size() );
    for(int i = 0; i < rect.size(); ++i) {
        corners_roi_W_h.col(i) << rect[i].x, rect[i].y, 0, 1;
    }

    // region of interest in desired camera pixels
    Matrix corners_roi_Cd_h = H * PW_C * corners_roi_W_h;
    Matrix corners_roi_Cd = corners_roi_Cd_h.colwise().hnormalized();
    cv::Vec4f bb_roi = boundingBox( corners_roi_Cd );

    // compute a scaling matrix to reduce image size befor warping
    float s = bb_roi[2] / src.cols;
    bb_roi /= s;

    // build tranlation and scaling matrix
    cv::Mat_<float> shiftscale = (cv::Mat_<float>(3, 3) << 1/s, 0, -bb_roi[0], 0, 1/s, -bb_roi[1], 0, 0, 1);

    // warp the image with corrected transfomation matrix
    cv::Mat cvH;
    cv::eigen2cv(H, cvH);
    cv::warpPerspective(src, dst, shiftscale * cvH, cv::Size( bb_roi[2], bb_roi[3] ));

    // compute the W to Cd and Cd to W mapping
    Matrix eigenShiftscale;
    cv::cv2eigen(shiftscale, eigenShiftscale);

    PW_Cd = eigenShiftscale * P * HCd_W.inverse();
    PCd_W = PC_W * H.inverse() * eigenShiftscale.inverse();
}
