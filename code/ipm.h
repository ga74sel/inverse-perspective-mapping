#ifndef IPM_H
#define IPM_H

#include <opencv2/opencv.hpp>
#include "utilities.h"

namespace ipm {

/*
 * This class warps a given image into a birds eye perspective by applying a inverse perspective mapping
 * call update function whenever a new Transformation between camera and floor becomes available
 * use warp to generate a warped image
 * use pixel2World / world2pixel to map between 3d world coordinates and image pixel coordinates
 */
class IPM
{
public:
    IPM(const Matrix& K = Matrix::Identity(3,3));

    void set_K( const Matrix& K );

    void set_roi( std::vector<cv::Point2f> roi );

    /* set new camera orientation and camera center */
    void update(float roll, float pitch, float yaw, const ipm::Vector& center);

    /* Convert image coordinates (2D non homogeneous pixel) to world coordinates (3D non homogeneouse Point) */
    void pixel2World(const std::vector< cv::Point2f >& x, std::vector< cv::Point3f >& X);
    void warp_pixel2World(const std::vector< cv::Point2f >& x, std::vector< cv::Point3f >& X);

    /* Convert world coordinates (3D non homogeneouse Point) to image coordinates (2D non homogeneous pixel) */
    void world2Pixel(const std::vector< cv::Point3f >& X, std::vector< cv::Point2f >& x);
    void warp_world2Pixel(const std::vector< cv::Point3f >& X, std::vector< cv::Point2f >& x);

    /* Apply inverse perspective mapping to image */
    void warp(const cv::Mat& src, cv::Mat& des);

private:
    Matrix K;
    Matrix P, invP;
    Matrix PC_W, PW_C;

    // Desired matrix
    Matrix RCd_W, H;
    Matrix HCd_W;
    Matrix PCd_W, PW_Cd;
    ipm::Vector Cd_W;

    std::vector<cv::Point2f> roi;
};

}

#endif // IPM_H
