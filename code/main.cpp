#include <opencv2/opencv.hpp>
#include "ipm.h"

using namespace std;
using namespace cv;
using namespace ipm;

ipm::IPM* globalIPM = NULL;

/*
 * Mouse callback function, used too display 3D coordinates of selected pixel
 */
static void onMouse( int event, int x, int y, int, void* )
{
    switch( event ) {
        case CV_EVENT_LBUTTONDOWN: {
            auto image_pts = std::vector< cv::Point2f >( { { (float)x, (float)y } } );
            std::vector< cv::Point3f > world_pts;
            globalIPM->warp_pixel2World( image_pts, world_pts);
            std::cout << "v=" << x << " u=" << y << " x=" << (float)world_pts[0].x << " y=" <<  (float)world_pts[0].y << " z=" << (float)world_pts[0].z << std::endl;
            break;
        }
        case CV_EVENT_RBUTTONDOWN:
            break;
        case CV_EVENT_LBUTTONUP:
            break;
        case CV_EVENT_RBUTTONUP:
            break;
    }
}

/*
 * Demo of IPM class, used for debugging / testing new features
 */
int main()
{
    // 1. load Image ( usally form nao camera )
    Mat img = imread("marker_50cm_high_res.png");
    Mat img_pose = img.clone();

    // 2. Define intrinsic camera parameters ( form camera callibration )
    ipm::Matrix K(3,3);
    K << 559.796267208495, 0, 320.836512688211, 0, 559.503857759651, 230.642331268047, 0, 0, 1;

    // 3. Define extrinsic camera parameters ( usally form nao robots tf tree )
    ipm::Vector C_W(3);
    float roll = -2.095; float pitch = 0.039; float yaw = -1.584;
    C_W << 0.040, -0.019, 0.487;
    C_W *= 1000;

    // 4. Create IMP object
    ipm::IPM ipm;
    // set Camera matrix
    ipm.set_K( K );
    // define region of interest by its for corner points in wolrd coordinates ( here 2.5m by 2.0m rect )
    ipm.set_roi( { {2500, 1000}, {2500, -1000}, {0, -1000}, {0, 1000} });

    // 5. setup cv windows
    namedWindow("directCamera", 0);
    namedWindow("warpedCamera", 0);
    setMouseCallback("warpedCamera", onMouse, 0);

    // 6. update imp and warp image
    ipm.update(roll, pitch, yaw, C_W);
    ipm.warp(img, img_pose);
    globalIPM = &ipm;

    while(1)
    {
        imshow("directCamera", img);
        imshow("warpedCamera", img_pose);
        if (cv::waitKey(30) == 27) {
             break;
        }
    }

    return 0;
}
