#ifndef POLYGON_CLIPPING_H
#define POLYGON_CLIPPING_H

#include <opencv2/opencv.hpp>
#include<iostream>
using namespace std;

// Returns x-value of point of intersectipn of two
// lines
cv::Point2f intersect(const cv::Point2f& p1, const cv::Point2f& p2,
                const cv::Point2f& p3, const cv::Point2f& p4)
{
    cv::Point2f p;

    float den = (p1.x-p2.x) * (p3.y-p4.y) - (p1.y-p2.y) * (p3.x-p4.x);
    p.x = ( (p1.x*p2.y - p1.y*p2.x) * (p3.x-p4.x) - (p1.x-p2.x) * (p3.x*p4.y - p3.y*p4.x) ) / den;
    p.y = ( (p1.x*p2.y - p1.y*p2.x) * (p3.y-p4.y) - (p1.y-p2.y) * (p3.x*p4.y - p3.y*p4.x) ) / den;

    return p;
}

void clip(std::vector<cv::Point2f>& poly_points, const cv::Point2f& p1, const cv::Point2f& p2)
{
    std::vector<cv::Point2f> new_points;

    // (ix,iy),(kx,ky) are the co-ordinate values of
    // the points
    for (int i = 0; i < poly_points.size(); i++)
    {
        // i and k form a line in polygon
        int k = (i+1) % poly_points.size();
        const cv::Point2f& pi = poly_points[i];
        const cv::Point2f& pk = poly_points[k];

        // Calculating position of first point
        // w.r.t. clipper line
        int i_pos = (p2.x-p1.x) * (pi.y-p1.y) - (p2.y-p1.y) * (pi.x-p1.x);

        // Calculating position of second point
        // w.r.t. clipper line
        int k_pos = (p2.x-p1.x) * (pk.y-p1.y) - (p2.y-p1.y) * (pk.x-p1.x);

        // Case 1 : When both points are inside
        if (i_pos < 0  && k_pos < 0)
        {
            //Only second point is added
            new_points.push_back( pk );
        }

        // Case 2: When only first point is outside
        else if (i_pos >= 0  && k_pos < 0)
        {
            // Point of intersection with edge
            // and the second point is added
            new_points.push_back( intersect( p1, p2, pi, pk ) );
            new_points.push_back( pk );
        }

        // Case 3: When only second point is outside
        else if (i_pos < 0  && k_pos >= 0)
        {
            new_points.push_back( intersect( p1, p2, pi, pk ) );
        }
    }
    poly_points = new_points;
}

// Implements Sutherland–Hodgman algorithm
void suthHodgClip(const std::vector<cv::Point2f>& poly_points, const std::vector<cv::Point2f>& clipper_points, std::vector<cv::Point2f>& new_points)
{
    new_points = poly_points;

    //i and k are two consecutive indexes
    for (int i=0; i<clipper_points.size(); i++) {
        int k = (i+1) % clipper_points.size();

        // We pass the current array of vertices, it's size
        // and the end points of the selected clipper line
        clip(new_points, clipper_points[i], clipper_points[k]);
    }
}
//https://www.geeksforgeeks.org/polygon-clipping-sutherland-hodgman-algorithm-please-change-bmp-images-jpeg-png/

#endif // POLYGON_CLIPPING_H
