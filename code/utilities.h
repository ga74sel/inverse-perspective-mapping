#ifndef UTILITIES_H
#define UTILITIES_H

#include <Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <Eigen/QR>

namespace ipm
{

typedef float Scalar;
typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> Matrix;
typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> Vector;

// Convert a vector of non-homogeneous 2D points to a vector of homogenehous 2D points.
inline void to_homogeneous(const std::vector< cv::Point2f >& non_homogeneous, Matrix& homogeneous) {
    homogeneous.resize(3, non_homogeneous.size());
    for( int i = 0; i < non_homogeneous.size(); ++i) {
        homogeneous.col(i) << non_homogeneous[i].x, non_homogeneous[i].y, 1.0f;
    }
}

// Convert a vector of homogeneous 2D points to a vector of non-homogenehous 2D points.
inline void from_homogeneous(const Matrix& homogeneous, std::vector< cv::Point2f >& non_homogeneous) {
    non_homogeneous.resize( homogeneous.cols() );
    for( int i = 0; i < non_homogeneous.size(); ++i) {
        non_homogeneous[i].x = homogeneous(i, 0) / homogeneous(i, 2);
        non_homogeneous[i].y = homogeneous(i, 1) / homogeneous(i, 2);
    }
}

// Convert a vector of non-homogeneous 3D points to a vector of homogenehous 4D points.
inline void to_homogeneous(const std::vector< cv::Point3f >& non_homogeneous, Matrix& homogeneous) {
    homogeneous.resize(4, non_homogeneous.size());
    for( int i = 0; i < non_homogeneous.size(); ++i) {
        homogeneous.col(i) << non_homogeneous[i].x, non_homogeneous[i].y, non_homogeneous[i].z, 1.0f;
    }
}

// Convert a vector of homogeneous 4D points to a vector of non-homogenehous 3D points.
inline void from_homogeneous(const Matrix& homogeneous, std::vector< cv::Point3f >& non_homogeneous) {
    non_homogeneous.resize( homogeneous.cols() );
    for( int i = 0; i < non_homogeneous.size(); ++i) {
        non_homogeneous[i].x = homogeneous(0, i) / homogeneous(3, i);
        non_homogeneous[i].y = homogeneous(1, i) / homogeneous(3, i);
        non_homogeneous[i].z = homogeneous(2, i) / homogeneous(3, i);
    }
}

// Build camera matrix W -> C
// K - Intrinsic Parameters, RC_W - Transf C -> W, C_W - Camera origin world
inline Matrix build_P(const Matrix& K, const Matrix& RC_W, const Matrix& C_W)
{
    Matrix P(3, 4);
    P << K * RC_W.transpose(), - RC_W.transpose() * C_W;
    return P;
}

// Build inverse camera matrix C -> W
// K - Intrinsic Parameters, RC_W - Transf C -> W, C_W - Camera origin world
inline Matrix build_invP(const Matrix& K)
{
    Matrix invP(4, 3);
    invP << K.inverse(), 0, 0, 1;
    return invP;
}

inline Matrix build_H(const Matrix& R, const Matrix t)
{
    Matrix H(4,4);
    H << R, t, 0, 0, 0, 1;
    return H;
}

inline Matrix build_R(double roll, double pitch, double yaw)
{
    Matrix Rx(3, 3), Ry(3,3), Rz(3,3);
    Rx << 1, 0, 0, 0, cos(roll), -sin(roll), 0, sin(roll), cos(roll);
    Ry << cos(pitch), 0, sin(pitch), 0, 1, 0, -sin(pitch), 0, cos(pitch);
    Rz << cos(yaw), -sin(yaw), 0, sin(yaw), cos(yaw), 0, 0, 0, 1;
    return Rz * Ry * Rx;
}

inline cv::Vec4f boundingBox(const Matrix& points)
{
    cv::Vec4f bb;
    bb[0] = points.row(0).minCoeff();
    bb[1] = points.row(1).minCoeff();
    bb[2] = points.row(0).maxCoeff() - bb[0];
    bb[3] = points.row(1).maxCoeff() - bb[1];
    return bb;
}

}

#endif // UTILITIES_H
